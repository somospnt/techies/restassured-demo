package com.somospnt.restassured;

import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static io.restassured.RestAssured.expect;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class PersonajeControllerTest {

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void before() {
        RestAssuredMockMvc.webAppContextSetup(context);
    }

    @AfterEach
    public void after() {
        RestAssuredMockMvc.reset();
    }

    @Test
    public void getPersonaje_existe_retornaPersonaje() {
        when()
                .get("/personajes/1")
                .then()
                .status(HttpStatus.OK)
                .body("id", equalTo(1))
                .body("serie.nombre", equalTo("Invasor Zim"));
    }

    @Test
    public void getPersonaje_noExiste_status500() {
        expect()
                .statusCode(500)
                .when()
                .get("/personajes/-100");
    }

}
