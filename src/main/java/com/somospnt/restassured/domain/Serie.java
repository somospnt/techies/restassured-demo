package com.somospnt.restassured.domain;

import lombok.Data;

@Data
public class Serie {
    private String nombre;
}
