package com.somospnt.restassured.domain;

import lombok.Data;

@Data
public class Personaje {

    private Long id;
    private String nombre;
    private Serie serie;

}
