package com.somospnt.restassured.controller;

import com.somospnt.restassured.domain.Personaje;
import com.somospnt.restassured.domain.Serie;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
public class PersonajeController {

    @GetMapping("/personajes/{id}")
    public Personaje getPersonaje(@PathVariable long id) {
        if (id != 1) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
        Personaje p = new Personaje();
        p.setId(1L);
        p.setNombre("Zim");
        Serie serie = new Serie();
        serie.setNombre("Invasor Zim");
        p.setSerie(serie);
        return p;
    }

}
